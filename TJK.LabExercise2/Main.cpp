// Playing Cards
// Tyler Kelley

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;



enum Rank
{
	Two = 2,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	Ace
};

enum Suit
{
	Hearts,
	Diamonds,
	Spades,
	Clubs
};

struct Card
{
	Suit Suit;
	Rank Rank;
};

void PrintCard(Card card) {
	switch (card.Rank) {
	case Two: cout << "The two of "; break;
	case Three: cout << "The Three of "; break;
	case Four: cout << "The Four of "; break;
	case Five: cout << "The Five of "; break;
	case Six: cout << "The Six of "; break;
	case Seven: cout << "The Seven of "; break;
	case Eight: cout << "The Eight of "; break;
	case Nine: cout << "The Nine of "; break;
	case Ten: cout << "The Ten of "; break;
	case Jack: cout << "The Jack of "; break;
	case Queen: cout << "The Queen of "; break;
	case King: cout << "The King of "; break;
	case Ace: cout << "The Ace of "; break;
	}
	switch (card.Suit) {
	case Spades: cout << "Spades" << endl; break;
	case Hearts: cout << "Hearts" << endl; break;
	case Diamonds: cout << "Diamonds" << endl; break;
	case Clubs: cout << "Clubs" << endl; break;
	}
	
}

Card HighCard(Card card1, Card card2) {
	if (card1.Rank > card2.Rank)
		return card1;
	return card2;

}


int main()
{
	Card c1;
	Card c2;
	c1.Rank = Two;
	c1.Suit = Spades;
	c2.Rank = Jack;
	c2.Suit = Clubs;
	PrintCard(HighCard(c1, c2));
	_getch();
	return 0;
}